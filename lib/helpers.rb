class Date
  def pretty_date
    strftime("%d %b %Y")
  end
end

class Time
  def pretty_date
    strftime("%d %b %Y")
  end
end

require 'resolv'
module EmailValidator
  def self.valid?(email)
    if email =~ /^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$/
      domain = email.match(/\@(.+)/)[1]
      Resolv::DNS.open do |dns|
          @mx = dns.getresources(domain, Resolv::DNS::Resource::IN::MX)
      end
      @mx.size > 0 ? true : false
    else
      false
    end
  end

end