function mercurySaveSettings(){
  jQuery(window).on('mercury:saved', function() {
    window.location = window.location.href.replace(/\/edit/i, '');
  });
}