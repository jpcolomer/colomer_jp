function mercurySaveSettings(){
  jQuery(window).on('mercury:saved', function() {
    jQuery.ajax({
      type: 'GET',
      url: '/last_post.json',
      dataType: 'json',
      success: function(data){
        window.location = window.location.href.replace(/new/i, data.id);
      }
    });
  });
}