require 'sinatra'
require "sinatra/content_for"
require 'mongoid'
require 'will_paginate_mongoid'
require 'digest/md5'
require 'cgi'

Dir[File.dirname(__FILE__) + '/lib/*.rb'].each {|file| require file }
Dir[File.dirname(__FILE__) + '/model/*.rb'].each {|file| require file }

configure :development do
  Mongoid.load!('./mongoid.yml', :development)
  enable :logging
  set :logging, Logger::DEBUG
  LOGGER = Logger.new("sinatra.log")
end

configure :production do
  Mongoid.load!('./mongoid.yml', :production)
end

enable :sessions

helpers do
  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]    
  end
  def signed_in?
    current_user
  end
  def recent_posts
    Post.recent.limit(3).to_a
  end
  def h(string)
    CGI::escapeHTML(string)
  end
end

get "/" do
  @posts = Post.recent.paginate(page: params[:page], per_page: 4) 
  erb :"posts/index"
end

#posts actions


get "/posts/new" do
  if signed_in?
    @post = Post.new
    erb :"posts/new"
  else
    redirect to('/log_in')
  end
end


post "/posts" do
  if signed_in?
    post_params = {title: params[:content][:post_title][:value], content: params[:content][:post_content][:value], description: params[:content][:post_description][:value]}
    post = Post.new(post_params)
    if post.save
      200
    else
      erb :"posts/new"
    end
  else
    redirect to('/log_in')
  end
end

get "/posts/:_id" do
  @post = Post.find(params[:_id])
  @comments_size = @post.comments.count
  raise h(@post.comments.last.content)
  erb :"posts/show"
end

get '/posts/:_id/edit' do
  if signed_in?
    @post = Post.find(params[:_id])
    erb :"posts/edit"
  else
    redirect to('/log_in')
  end
end

put "/posts/:_id" do
  if signed_in?
    @post = Post.find(params[:_id])
    post_params = {title: params[:content][:post_title][:value], content: params[:content][:post_content][:value], description: params[:content][:post_description][:value]}
    if @post.update_attributes(post_params)
      200
    else
      erb :"posts/edit"
    end
  else
    redirect to('/log_in')
  end
end

delete "/posts/:_id" do
  if signed_in?
    Post.find(params[:_id]).destroy
    redirect to('/')
  else
    raise "No tiene permiso"
  end
end


get "/last_post.json" do
  if signed_in?
    content_type :json
    {:id => recent_posts.first._id }.to_json
  else
    raise "No tiene permiso"
  end
end

#session actions
get "/log_in" do
  erb :"sessions/new"
end

post "/log_in" do
  begin
    user = User.authenticate(params[:email], params[:password])
    session[:user_id] = user._id
    redirect to('/')
  rescue
    "Invalid email or password"
  end
end

delete '/admin/session' do
  session[:user_id] = nil
  redirect to('/')
end

post "/posts/:_id/comments" do
  post = Post.find params[:_id]


  post_comment = post.comments.new(params[:comment]) do |comment|
    comment.created_at = Time.now
    params[:author][:email].downcase!
    hash = Digest::MD5.hexdigest params[:author][:email]
    params[:author][:gravatar_url] = "http://www.gravatar.com/avatar/#{hash}"
    comment.author_attributes = params[:author]
  end

  if post_comment.save
    redirect back
  else
    "Invalid Email"
  end

end