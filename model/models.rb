class Post
  include Mongoid::Document

  embeds_many :comments
  field :content, type: String
  field :title, type: String
  field :description, type: String
  field :created_at, type: Time

  scope :recent, desc(:created_at)

  class << self
    def new(options={})
      options = options.merge({created_at: Time.now})
      super(options)
    end
  end

  def pretty_date
    created_at.pretty_date
  end

  def create_comment
  end

end

require 'bcrypt'

class User
  include Mongoid::Document

  field :name, type: String
  field :nickname, type: String
  field :password_salt, type: String
  field :password_hash, type: String
  field :email, type: String


  {create: :define_singleton_method, update_attributes: :define_method}.each do |method_name, type_definition|
    send(type_definition, method_name) do |options|
      if options.include?(:password) && options[:password]
        password = options.delete(:password)
        super options.merge(User.send(:encrypt_password,password))
      else
        raise 'Password Error'
      end
    end
  end 


  class << self

    def authenticate(email, password)
      user = find_by(email: email)
      if user && user.password_hash == BCrypt::Engine.hash_secret(password, user.password_salt)
        user
      else
        nil
      end

    end

    private
    def encrypt_password(password)
      password_salt = BCrypt::Engine.generate_salt
      {password_salt: password_salt, password_hash: BCrypt::Engine.hash_secret(password, password_salt)}
    end

  end

end



class Comment
  include Mongoid::Document



  field :content, type: String
  field :created_at, type: Time
  embeds_one :author, class_name: "CommentAuthor"
  embedded_in :post
  accepts_nested_attributes_for :author

  validates_presence_of :content

  # class << self
  #   def new(options={})
  #     options[:created_at] = Time.now
  #     super(options)
  #   end
  # end

end

class CommentAuthor
  include Mongoid::Document
  embedded_in :comment
  field :name, type: String
  field :email, type: String
  field :gravatar_url, type: String


  validates_presence_of :name, :email, :gravatar_url
  validate :email_format

  # class << self

  #   def new(options={})
  #     options[:email].downcase!
  #     hash = Digest::MD5.hexdigest options[:email]
  #     options[:gravatar_url] = "http://www.gravatar.com/avatar/#{hash}"
  #     super(options)
  #   end

  # end


  private

  def email_format
    unless EmailValidator::valid? self.email
      self.errors.add(:email, 'not valid')
    end
  end

end